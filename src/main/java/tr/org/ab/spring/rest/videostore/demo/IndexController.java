package tr.org.ab.spring.rest.videostore.demo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/")
public class IndexController {

    @RequestMapping("/test")
    Object hello(){
        return Collections.singletonMap("message", "hello world");
    }


}
